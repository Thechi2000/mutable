# Mutable

Mutable is a crate to keep track of changes in structures\
It is still very much WIP

Here is a small example:

```rust
use mutable::Mutable;
use mutable_derive::Mutable;

#[derive(Mutable)]
struct Simple {
    size: usize,
}

#[derive(Mutable)]
struct Complex {
    id: String,
    value: Simple,
}

fn main() {
    let mut c0 = Complex { id: "a".to_string(), value: Simple { size: 32 } };
    let c1 = Complex { id: "b".to_string(), value: Simple { size: 64 } };

    assert_eq!(c0.update(c1), vec![
        ComplexMutation::Id(("a".to_string(), "b".to_string())),
        ComplexMutation::Value(SimpleMutation::Size((32, 64)))
    ]);
}
```