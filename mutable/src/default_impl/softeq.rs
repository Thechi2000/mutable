#[cfg(feature = "uuid")]
use uuid::Uuid;

use crate::cmp::SoftEq;

impl SoftEq for i8 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for i16 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for i32 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for i64 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for i128 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for isize {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for u8 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for u16 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for u32 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for u64 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for u128 {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for usize {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for bool {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for () {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
impl SoftEq for String {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        self.clone()
    }
}

#[cfg(feature = "uuid")]
impl SoftEq for Uuid {
    type Uid = Self;
    fn uid(&self) -> Self::Uid {
        *self
    }
}
