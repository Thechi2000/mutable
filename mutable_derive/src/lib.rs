//! Just a crate for the derive macros

mod derive_mutable;
mod derive_softeq;

use proc_macro::TokenStream;
use proc_macro_error::proc_macro_error;


/// Generate the implementation for the [Mutable](../mutable/trait.Mutable.html) trait
///
/// May only be used on enums
/// Requires that all fields implements [Mutable](../mutable/trait.Mutable.html)
///
/// The mutation type is an enum, for which each variant correspond to a field of the struct.
/// Each of them as only one element, which is the mutation type of the attribute type (see [Mutable](../mutable/trait.Mutable.html#mutation-generation))
#[proc_macro_derive(Mutable)]
#[proc_macro_error]
pub fn derive_mutable(item: TokenStream) -> TokenStream {
    if let Ok(ts) = derive_mutable::try_(item) {
        ts
    } else {
        panic!()
    }
}

/// Generate the implementation for the [SoftEq](../mutable/trait.SoftEq.html) trait
///
/// The elements of the hard part must be annoted with `#[softeq(uid)`\
/// At least one hard element is required
#[proc_macro_derive(SoftEq, attributes(softeq))]
#[proc_macro_error]
pub fn derive_softeq(item: TokenStream) -> TokenStream {
    if let Ok(ts) = derive_softeq::try_(item) {
        ts
    } else {
        panic!()
    }
}