//! Test for [Mutable](../mutable/trait.Mutable.html) and [SoftEq](../mutable/trait.SoftEq.html)

mod mutable;
mod mutation_holder;
mod softeq;
