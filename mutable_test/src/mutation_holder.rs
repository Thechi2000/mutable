#[cfg(test)]
mod test {
    use mutable::holder::MutableHolder;
    use mutable_derive::Mutable;

    #[derive(Mutable, Clone)]
    struct Foo {
        int: i32,
    }

    #[derive(Mutable, Clone)]
    struct Bar {
        foo: Foo,
        int: i32,
    }

    #[test]
    fn mutable_holder() {
        let mut mh = MutableHolder::new(Bar {
            foo: Foo { int: 2 },
            int: 3,
        });

        mh.get_mut().int = 4;
        assert_eq!(mh.get().int, 4);
        assert_eq!(mh.mutations(), vec![BarMutation::Int((3, 4))]);

        mh.get_mut().foo.int = 1;
        assert_eq!(mh.get().foo.int, 1);
        assert_eq!(
            mh.mutations(),
            vec![BarMutation::Foo(FooMutation::Int((2, 1)))]
        );
    }
}
