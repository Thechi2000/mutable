#[allow(dead_code)]
#[cfg(test)]
mod test {
    use mutable::cmp::SoftEq;
    use mutable_derive::SoftEq;

    #[derive(SoftEq)]
    struct Item {
        #[softeq(uid)]
        id: String,
        charge: usize,
        slot: u8,
    }

    #[test]
    fn soft_eq() {
        let i0 = Item {
            id: "tear".to_string(),
            charge: 32,
            slot: 3,
        };
        let i1 = Item {
            id: "tear".to_string(),
            charge: 12,
            slot: 3,
        };
        let i2 = Item {
            id: "draktaar".to_string(),
            charge: 32,
            slot: 4,
        };
        let i3 = Item {
            id: "draktaar".to_string(),
            charge: 32,
            slot: 3,
        };

        assert!(i0.se(&i1));
        assert!(i1.se(&i0));

        assert!(i2.se(&i3));
        assert!(i3.se(&i2));

        assert!(!i0.se(&i2));
        assert!(!i2.se(&i0));

        assert!(!i0.se(&i3));
        assert!(!i3.se(&i0));

        assert!(!i1.se(&i2));
        assert!(!i2.se(&i1));

        assert!(!i1.se(&i3));
        assert!(!i3.se(&i1));
    }
}
